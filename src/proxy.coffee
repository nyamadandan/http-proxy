config = require 'config'
http = require 'http'
url = require 'url'

class HttpProxy
  constructor: (manager) ->
    proxyHandler = (requestFromClient, responseToClient) =>
      if manager?
        connectionManager = manager.createConnectionManager().onClientStart(requestFromClient)

      parsedUrl = url.parse(requestFromClient.url)

      headersToServer = {}
      for own key, value of requestFromClient.headers
        if key isnt 'host'
          headersToServer[key] = value
        else
          headersToServer[key] = config.proxy.destination_host or parsedUrl.post

      options =
        method : requestFromClient.method
        headers : headersToServer
        host : config.proxy.destination_host or parsedUrl.host
        path : config.proxy.destination_path or parsedUrl.path
        port : config.proxy.destination_port or parsedUrl.port

      requestToServer = http.request(options, (responseFromServer) =>
        connectionManager.onServerStart(responseFromServer) if connectionManager?

        serverBufferLength = 0
        serverChunks = []
        serverBody = ''
        responseFromServer.on('data', (chunk) =>
          serverBufferLength += chunk.length
          serverChunks.push(chunk)
          serverBody += chunk
          0
        )

        responseFromServer.on('end', () =>
          offset = 0
          serverBuffer = new Buffer(serverBufferLength, 'ascii')
          serverChunks.forEach( (chunk) ->
            chunk.copy(serverBuffer, offset)
            offset += chunk.length
          )

          if connectionManager?
            connectionManager.onServerEnd(serverBuffer, (buffer, statusCode, headers) =>
              #コールバック関数によるフィルタリングを使用してバッファを返す
              responseToClient.statusCode = statusCode
              for own key, value of headers
                responseToClient.setHeader(key, value)
              responseToClient.end(buffer)
            )
          else
            #そのまま返す
            responseToClient.statusCode = responseFromServer.statusCode
            for own key, value of responseFromServer.headers
              responseToClient.setHeader(key, value)

            responseToClient.end(serverBuffer)
          0
        )
        0
      )

      clientBufferLength = 0
      clientChunks = []
      requestFromClient.on('data', (chunk) =>
        clientBufferLength += chunk.length
        clientChunks.push(chunk)
        0
      )

      requestFromClient.on('end', () =>
        offset = 0
        clientBuffer = new Buffer(clientBufferLength, 'ascii')
        clientChunks.forEach( (chunk) ->
          chunk.copy(clientBuffer, offset)
          offset += chunk.length
        )

        connectionManager.onClientEnd(clientBuffer) if connectionManager?

        requestToServer.end(clientBuffer)
        0
      )
      0

    http.createServer()
      .on('request', proxyHandler)
      .listen(config.proxy.http_port, () ->
        console.log("Proxy server listening on port #{config.proxy.http_port}")
      )
    @

module.exports = HttpProxy

