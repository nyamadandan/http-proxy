config = require 'config'

express = require 'express'
socketio = require('socket.io')
routes = require './routes'
http = require 'http'
path = require 'path'

class ConnectionManager
  constructor: (@manager, @sessionId) ->
    @responseProxy = {}
    @done = false
    @

  onClientStart: (@requestFromClient) ->
    body = {
      sessionId : @sessionId
      url : @requestFromClient.url
    }
    @manager.io.sockets.emit('client_start', body)
    @

  onClientEnd: (@clientBuffer) ->
    @

  onServerStart: (responseFromServer) ->
    @responseFromServer = responseFromServer
    @

  doResponse : () ->
    if @done
      return @

    if not @serverBuffer
      return @

    if @timeout?
      clearTimeout(@timeout)
      @timeout = null

    responseBuffer = @responseProxy.buffer or @serverBuffer
    statusCode = @responseProxy.statusCode or @responseFromServer.statusCode
    headers = @responseProxy.headers or @responseFromServer.headers

    @serverEndCallback(responseBuffer, statusCode, headers)

    @manager.io.sockets.emit('remove', {
      sessionId : @sessionId
    })
    @manager.removeConnectionManager(@)
    @done = true
    @

  onServerEnd: (@serverBuffer, @serverEndCallback) ->
    body = {
      sessionId : @sessionId
      url : @requestFromClient.url
    }

    @manager.io.sockets.emit('server_end', body)

    if @manager.responseDelay is 0
      @doResponse()
    else if @responseProxy.headers? or @responseProxy.buffer? or @responseProxy.statusCode?
      @doResponse()
    else
      @timeout = setTimeout(() =>
        @doResponse()
        0
      , @manager.responseDelay)
    @

class Manager
  constructor: () ->
    @connectionManagers = {}
    @sessionId = 0
    @responseDelay = 0

    app = express()
    app.configure(() ->
      app.set('port', config.manager.http_port)
      app.set('views', path.join(__dirname, '..', 'views'))
      app.set('view engine', 'jade')
      app.use(require('stylus').middleware(path.join(__dirname, '..', 'public')))
      app.use(express.logger('dev'))
      app.use(express.bodyParser())
      app.use(express.methodOverride())
      app.use(app.router)
      app.use(express.static(path.join(__dirname, '..', 'public')))
      0
    )

    app.configure('development', () ->
      app.use(express.errorHandler())
    )

    app.get('/', routes.top.index)

    server = http.createServer(app)
      .listen(config.manager.http_port, () ->
        console.log("Http server listening on port #{config.manager.http_port}")
      )

    @io = socketio.listen(server)

    @io.sockets.on('connection', (socket) =>
      socket.emit('init', {
        delay : @responseDelay
      })

      #on forbid
      socket.on('set global delay', (data) =>
        @responseDelay = parseInt(data.delay, 10)
        0
      )

      #on forbid
      socket.on('ok', (data) =>
        connectionManager = @connectionManagers[data.sessionId]
        if connectionManager?
          connectionManager.doResponse()
        0
      )

      #on forbid
      socket.on('403', (data) =>
        connectionManager = @connectionManagers[data.sessionId]
        if connectionManager?
          buffer = new Buffer('403 FORBIDDEN', 'ascii')
          responseProxy =
            statusCode : 403
            buffer : buffer
            headers : {'Content-Length' : buffer.length}
          connectionManager.responseProxy = responseProxy
          connectionManager.doResponse()
        0
      )
    )
    @

  createConnectionManager: () ->
    connectionManager = new ConnectionManager(@, @sessionId)
    @connectionManagers[@sessionId] = connectionManager
    @sessionId += 1
    connectionManager

  removeConnectionManager: (connectionManager) ->
    delete @connectionManagers[connectionManager.sessionId]
    @

module.exports = Manager

